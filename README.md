# raspi-ansible

Ansible configurations for fresh RaspberryPi setup. This project was done for Ansible version 2.5.

* Install git (`sudo apt-get install git`)
* Clone this repository on the target machine
* Install ansible (`sudo apt-get install ansible`)
* Execute desired ansible playbook in `/playbooks` with `ansible-playbook -K playbook.yml`